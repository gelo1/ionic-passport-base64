
To run in the real device:
* Clone this repo
* Run `npm install`
* Run `ionic cordova platform rm ios`
* Run `ionic cordova platform add ios`
* Run `ionic cordova platform rm android`
* Run `ionic cordova platform add android@6.3.0`
* Run `ionic cordova run ios`
* Run `ionic cordova run android`
